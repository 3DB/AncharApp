package chargeapp.anchar.util

import java.text.SimpleDateFormat
import java.util.*

class TimeUtils{
    companion object {
        fun toDate(timestamp: Long): String {
            val dv: Long = ((timestamp) * 1000) // Faktor 1000 wieder implementieren
            val df = java.util.Date(dv)
            return SimpleDateFormat("EEE, d MMM yyyy 'um' HH:mm" , Locale.getDefault()).format(df)
        }
        fun formatDuration(startTimestamp: Long, endTimestamp: Long): String {
            var absSeconds = Math.abs(endTimestamp!! - startTimestamp)
            return formatDuration(absSeconds)
        }

        fun formatDuration(absSeconds: Long): String {
            val str = String.format(
                    "%d Std. %02d Min.",
                    absSeconds / 3600,
                    (absSeconds % 3600) / 60)
            return str
        }
    }
}