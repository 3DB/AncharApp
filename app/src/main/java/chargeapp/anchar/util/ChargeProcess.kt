package chargeapp.anchar.util

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import chargeapp.anchar.models.AliasSignature
import chargeapp.anchar.models.ChargeInfo
import chargeapp.anchar.models.ChargeProcessData
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.http.HTTPMethods
import com.android.volley.VolleyError
import org.json.JSONObject
import java.util.*


class ChargeProcess(val context: Context,
                    val data: ChargeProcessData,
                    val http: HTTPMethods = HTTPMethods.getInstance(context)) {
    companion object {
        const val USERSIG = "ALIAS"
        const val STATIONID = "STATIONID"
        const val URL = "URL"
        const val RECEIVEDDATA = "RECEIVEDDATA"

        fun tokenGenerator(length: Int = 32): String
        {
            val alphabet = "01234567989ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmopqrstuvwxyz"
            var token = ""
            for (i in 1..length)
            {
                val ranChar = Random().nextInt(alphabet.length - 1)
                token += alphabet.get(ranChar)
            }
            return token
        }

        fun newInstance(ipAddressHome: String, ipAddressForeign: String, stationId: Int, randID: String,  context: Context): ChargeProcess{
            val cpd = ChargeProcessData(ipAddressForeign, ipAddressHome, stationId, (Calendar.getInstance().time).toString(), randID) // TODO remember what a key was used to do
            return ChargeProcess(context, cpd)
        }

        fun clearCharging(context: Context) {
            val sp: SharedPreferences =  context.getSharedPreferences("isCharging", Context.MODE_PRIVATE) // TODO change to unique
            val spe: SharedPreferences.Editor = sp.edit()
            spe.clear()
            spe.apply()
        }

        fun isCharging(context: Context): Boolean {
            val sp: SharedPreferences =  context.getSharedPreferences("isCharging", Context.MODE_PRIVATE)
            return sp.getString(ChargeProcess.USERSIG, "") != ""
        }

        fun receivedData(context: Context) {
            val sp: SharedPreferences =  context.getSharedPreferences("isCharging", Context.MODE_PRIVATE) // TODO change to unique
            val spe: SharedPreferences.Editor = sp.edit()
            spe.putBoolean(ChargeProcess.RECEIVEDDATA, true)
            spe.apply()
        }

        fun getChargingInfo(context: Context, errorHandler: (error: VolleyError) -> Unit, completionHandler: (ChargeInfo) -> Unit) {
            val jsonData = JSONObject()
            val sp: SharedPreferences =  context.getSharedPreferences("isCharging", Context.MODE_PRIVATE)

            jsonData.put("station_id", sp.getInt(STATIONID, 0))
            jsonData.put("user_signature", sp.getString(USERSIG, ""))

            HTTPMethods.getInstance(context).post(
                    sp.getString(ChargeProcess.URL, ""),
                    "/user_charging_status",
                    jsonData
                    ) {
                completionHandler(ChargeInfo.fromJson(it))
            }
        }

        fun isDataReceived(context: Context): Boolean {
            val sp: SharedPreferences = context.getSharedPreferences("isCharging", Context.MODE_PRIVATE)
            return sp.getBoolean(RECEIVEDDATA, false)
        }

    }

    fun beginCharging(errorFunc: (error: VolleyError) -> Unit, completionHandler: (response: Contract) -> Unit) {
        val jsonParam = JSONObject()
        jsonParam.put("station_id", data.stationId.toString())
        jsonParam.put("token", data.token)
        jsonParam.put("rand_id", data.randID)
        Log.i("ip", data.ipAddressForeignServer)
        http.post(data.ipAddressForeignServer, "/begin_charging", jsonParam, errorHandler = errorFunc) { response ->
            data.contract = Contract.fromJson(response)
            data.aliasSignature!!.contract_signature = Crypto.signBase64(data.keyAlias, data.contract!!.userHash)

            completionHandler(data.contract!!)
        }
    }

    fun requestAlias(errorHandler: (error: VolleyError) -> Unit, completionHandler: () -> Unit) {
        val json = data.aliasSignature!!.toJson()
        http.post(data.ipAddressHomeServer,
                        "/sign_alias",
                        json,
                sendAuth = true) {
            response: JSONObject -> data.aliasSignature = AliasSignature.fromJson(response)
            completionHandler()
        }
    }

    fun acceptContract(errorHandler: (error: VolleyError) -> Unit, completionHandler: (response: JSONObject) -> Unit){
        val jsonData = JSONObject()
        jsonData.put("alias_signature", data.aliasSignature!!.toJson().toString())
        jsonData.put("token", data.token)
        http.post(data.ipAddressForeignServer, "/accept_contract", jsonData) {
            response: JSONObject -> completionHandler(response)
        }
    }



    fun setCharging() {
        val sp: SharedPreferences =  context.getSharedPreferences("isCharging", Context.MODE_PRIVATE) // TODO change to unique
        val spe: SharedPreferences.Editor = sp.edit()
        spe.clear()
        spe.putString(ChargeProcess.USERSIG, this.data.aliasSignature!!.contract_signature)
        spe.putInt(ChargeProcess.STATIONID, this.data.stationId)
        spe.putString(ChargeProcess.URL, this.data.ipAddressForeignServer)
        spe.putBoolean(ChargeProcess.RECEIVEDDATA, false)
        spe.apply()
    }



    /**
     * get Alias and accept with that Alias the Contract
     */
    fun requestAliasAndAccept(errorHandler: (error: VolleyError) -> Unit, completionHandler: () -> Unit)
    {
        requestAlias(errorHandler) {
            acceptContract(errorHandler)
            {
                this.setCharging()
                completionHandler()
            }
        }
    }
}
