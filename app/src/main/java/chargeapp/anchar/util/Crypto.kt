package chargeapp.anchar.util

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Log
import java.security.*
import java.util.*


class Crypto{
    companion object {

        fun genKeyPair(alias: String): KeyPair {
            val kpg: KeyPairGenerator = KeyPairGenerator.getInstance(
                    KeyProperties.KEY_ALGORITHM_EC,
                    "AndroidKeyStore"
            )
            val parameterSpec: KeyGenParameterSpec = KeyGenParameterSpec.Builder(
                    alias,
                    KeyProperties.PURPOSE_SIGN or KeyProperties.PURPOSE_VERIFY
            ).run {
                setKeySize(521)
                setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
                build()
            }

            kpg.initialize(parameterSpec)

            val kp = kpg.generateKeyPair()

            return kp
        }

        /**
         * Load the Android KeyStore instance using the
         * "AndroidKeyStore" provider to list out what entries are
         * currently stored.
         */
        fun listKeys(): Enumeration<String> {

            val ks: KeyStore = KeyStore.getInstance("AndroidKeyStore").apply {
                load(null)
            }
            return ks.aliases()
        }

        fun delKeys() {
            listKeys().iterator().forEach {
                delKey(it)
            }
        }

        fun delKey(alias: String)
        {
            val ks: KeyStore = KeyStore.getInstance("AndroidKeyStore").apply {
                load(null)
            }
            ks.deleteEntry(alias)
        }

        /**
         * Use a PrivateKey in the KeyStore to create a signature over
         * some data.
         */
        fun sign(alias: String, data: String): ByteArray {

            Log.i("Crypto", "Try to sign with $alias")
            val ks: KeyStore = KeyStore.getInstance("AndroidKeyStore").apply {
                load(null)
            }
            val entry: KeyStore.Entry = ks.getEntry(alias, null)
            if (entry !is KeyStore.PrivateKeyEntry) {
                Log.w("Crypto", "Not an instance of a PrivateKeyEntry")
                throw RuntimeException("Not an instance of a PrivateKeyEntry")
            }

            val signature: ByteArray = Signature.getInstance("SHA256withECDSA").run {
                initSign(entry.privateKey)
                update(data.toByteArray())
                sign()
            }

            return signature
        }

        fun signBase64(alias: String, plainText: String): String {
            return byteArrayToBase64(sign(alias, plainText))
        }

        fun keyToBase64(key: PublicKey): String{
            return Base64.encodeToString(key.encoded, Base64.DEFAULT)
        }

        fun getPubKeyBase64(alias: String): String {
            val ks: KeyStore = KeyStore.getInstance("AndroidKeyStore" ).apply {
                load(null)
            }
            return keyToBase64(ks.getCertificate(alias).publicKey)
        }

        fun byteArrayToBase64(byteArray: ByteArray): String {
            return Base64.encodeToString(byteArray, Base64.DEFAULT)
        }

        fun hash(s: String): String{
            val md = MessageDigest.getInstance("SHA-256")
            md.update(s.toByteArray())
            val bytes = md.digest()
            return byteArrayToBase64(bytes)
        }
    }


}
