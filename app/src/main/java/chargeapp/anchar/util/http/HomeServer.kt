package chargeapp.anchar.util.http

import android.content.Context
import android.util.Log
import android.widget.Toast
import chargeapp.anchar.R
import chargeapp.anchar.models.Const
import chargeapp.anchar.models.Contract
import chargeapp.anchar.models.Station
import chargeapp.anchar.util.db.ContractRepo
import com.android.volley.VolleyError
import org.json.JSONArray
import org.json.JSONObject

class HomeServer(val context: Context, val http: HTTPMethods = HTTPMethods.getInstance(context)) {

    val sp = context.getSharedPreferences("login", Context.MODE_PRIVATE)

    fun errorLogin(error: VolleyError)
    {
        if(error.message != null) {
            Log.i("HomeServer", error.message)
        }
    }

    fun login(errorHandler: (VolleyError) -> Unit = {e -> errorLogin(e)}, completionHandler: () -> Unit)
    {
        val network = sp.getString(Const.networkString, "")

        http.post(network, "/login", sendAuth=true, errorHandler = errorHandler) {
            completionHandler()
        }
    }

    fun getContracts(startContract: Int = 0 , maxContract: Int = 100, errorHandler: (VolleyError) -> Unit = {e -> errorLogin(e)}, completionHandler: (it: List<Contract>) -> Unit) {
        val network = sp.getString(Const.networkString, "")

        val json = JSONObject()
        json.put("start_id", startContract)
        json.put("count", maxContract)

        http.post(network, "/get_contracts", sendAuth = true, errorHandler = errorHandler, postJson = json) {

            Log.i("HomeServer", it.toString())
            val list = ArrayList<Contract>();
            val jsonArrStinrg = it.getString("contracts")
            val jsonArr = JSONArray(jsonArrStinrg)

            for (i in 0..(jsonArr.length()) - 1) {
                val contract = Contract.fromJson(JSONObject(jsonArr.getString(i)))

                if (contract.startTimestamp in 1553990401..1554335999) {
                    contract.name = "JuFo Leverkusen"
                }
                else if (contract.startTimestamp > 1557964800) {
                    contract.name = "JuFo Chemnitz"
                }

                list.add(contract)
            }

            completionHandler(list)
        }
    }


    /**
     * @return if new Contract received, returns true.
     */
    fun putContractsInDB(newContractHandler: (Contract) -> Unit = {}) {
        Log.d("HomeServer","database not null")
        val cr = ContractRepo(context)

        this.getContracts {
            newContracts: List<Contract> ->
            for (contract: Contract in newContracts) {
                cr.insert(contract, newContractHandler)
            }
        }
    }

    fun getMapData(completionHandler: (List<Station>) -> Unit) {
        val network = sp.getString(Const.networkString, "")

        http.post(network, "/get_charging_station", sendAuth = true) {
            it.getJSONArray("charging_stations")
        }
    }
}
