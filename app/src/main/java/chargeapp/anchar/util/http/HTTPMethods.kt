package chargeapp.anchar.util.http

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import android.util.Log
import chargeapp.anchar.models.Const
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import org.json.JSONObject
import kotlin.collections.HashMap
import com.android.volley.DefaultRetryPolicy


class HTTPMethods constructor(val context: Context){
    companion object {
        @Volatile
        private var INSTANCE: HTTPMethods? = null

        fun getInstance(context: Context) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE
                            ?: HTTPMethods(context)
                }
    }

    val sp: SharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE)

    private val queue = RequestQueueHandler.getInstance(context)

    fun getUrl(hostname: String, path:String):String =  "http://$hostname$path"

    /**
     * Method for error handling
     */
    fun errorFunc(error: VolleyError): Unit
    {
        if (error.message != null)
            Log.i("HTTPMethods", error.message)
        else
            Log.i("HTTPMethods", error.toString())
    }

    /**
     * Method for http postString
     * @param hostname Domain/ip address of the server with port, e.g. server.com:7000
     * @param path Path of the api, e.g. /beginCharging
     * @param params Params for the postString request
     * @param completionHandler Handler, when postString completed
     * @param errorHandler Handler, when postString failed, can be empty
     */
    fun postString(hostname: String,
                   path: String, postParams: HashMap<String, String>,
                   errorHandler: (error: VolleyError) -> Unit = { error -> errorFunc(error)},
                   completionHandler: (response: String) -> Unit)
    {
        Log.d("HTTPMethods", "Tring Http Post to ${getUrl(hostname ,path)}")
        val stringRequest = object : StringRequest(Method.POST, getUrl(hostname, path),
                Response.Listener<String> { response ->
                    completionHandler(response)
                },
                Response.ErrorListener { error ->
                    errorHandler(error)
                }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                return postParams
            }
        }

        queue.addToRequestQueue(stringRequest)
    }

    /**
     * Method to post data
     * @param hostname Hostname of the server/URL
     * @param path Path to method e.g. /login
     * @param postJson Data which should be posted, can be empty
     * @param sendAuth Sends Auth with the Request, <b>should only be used with the HomeServer!</b>
     * @param errorHandler ErrorHandler
     * @param completionHandler completionHandler
     * @param authPassword Password for beginCharging if empty SharedPreferences will be used
     * @param authUsername Username for beginCharging if empty SharedPreferences will be used
     */
    fun post(hostname: String,
             path: String,
             postJson: JSONObject = JSONObject(),
             sendAuth: Boolean = false,
             authUsername: String? = null,
             authPassword: String? = null,
             errorHandler: (error: VolleyError) -> Unit = {error -> errorFunc(error)},
             completionHandler: (response: JSONObject) -> Unit)
    {
        Log.d("HTTPMethods", "Tring Http JSONPost to ${getUrl(hostname ,path)}")
        val jsonRequest = object : JsonObjectRequest(Method.POST, getUrl(hostname, path), postJson,
            Response.Listener<JSONObject> { response: JSONObject ->
                completionHandler(response)
            },
            Response.ErrorListener(errorHandler)
            )
            {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val params = HashMap<String, String>()
                    params["Content-Type"] = "application/json"
                    if (sendAuth) {
                        var email = sp.getString(Const.emailString, "")
                        var password = sp.getString(Const.passwordString, "")
                        if (authUsername != null)
                            email = authUsername

                        if (authPassword != null)
                            password = authPassword

                        if (password == "" || email == ""){
                            Log.i("HomeServer", "password/email/network was empty")
                        }
                        val credentials = "$email:$password"
                        val auth = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
                        params["Authorization"] = auth
                    }
                    params["Content-Type"] = "application/json"
                    return params
                }
            }

        jsonRequest.setRetryPolicy(DefaultRetryPolicy(20000, 0, 10f))
        queue.addToRequestQueue(jsonRequest)
    }

}

