package chargeapp.anchar.util.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import chargeapp.anchar.models.db.ContractDao
import android.arch.persistence.room.Room
import android.content.Context
import chargeapp.anchar.models.Contract


@Database(entities = [Contract::class], version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun ContractDau(): ContractDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        internal fun getDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                AppDatabase::class.java, "app_database")
                                .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}
