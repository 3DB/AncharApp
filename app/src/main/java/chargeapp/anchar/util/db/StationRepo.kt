package chargeapp.anchar.util.db

import android.arch.persistence.room.Dao
import chargeapp.anchar.models.Contract
import chargeapp.anchar.models.Station
import chargeapp.anchar.models.db.StationDao
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/*
@Dao
class StationRepo {

    fun insert(station: Station, successAdd: (Station) -> Unit = {}) {
        doAsync {
            if (!isStationIn(station)) {
                StationDao.insert(station)
                uiThread {
                    successAdd(it)
                }
            }
        }
    }

    /**
     * Should be used with ASYNC!
     */
    fun isStationIn(station: Station): Boolean {
        return StationDao.getContractWithFinalHash(contract.finalHash!!).isNotEmpty()
    }


}
*/