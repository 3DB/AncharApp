package chargeapp.anchar.util.db

import android.content.Context
import chargeapp.anchar.models.Contract
import chargeapp.anchar.models.db.ContractDao
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class ContractRepo {

    private var contractDao: ContractDao
    private var db: AppDatabase


    constructor(context: Context) {
        db = AppDatabase.getDatabase(context)!!
        contractDao = db.ContractDau()
    }


    fun insert(contract: Contract, successAdd: (Contract) -> Unit = {}) {
        doAsync {
            if (!isContractIn(contract)) {
                contractDao.insert(contract)
                uiThread {
                    successAdd(contract)
                }
            }
        }
    }

    fun getContracts(func: (List<Contract>) -> Unit) {
        doAsync {
            val contracts = contractDao.getAll()
            uiThread {
                func(contracts)
            }
        }
    }

    /**
     * Should be used with ASYNC!
     */
    fun isContractIn(contract: Contract): Boolean {
        return contractDao.getContractWithFinalHash(contract.finalHash!!).isNotEmpty()
    }

    fun deleteAll() {
        doAsync {
            contractDao.nukeTable()
        }
    }



}