package chargeapp.anchar.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import chargeapp.anchar.R
import android.content.Context.NOTIFICATION_SERVICE
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import chargeapp.anchar.models.Contract

class Notifactions {
    companion object {
        val PRIMARY_CHANNEL_ID = "de.anchar"
    }

    private var mNotifyManager: NotificationManager? = null

    fun createNotificationChannel(context: Context) {
        mNotifyManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >=
                android.os.Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(PRIMARY_CHANNEL_ID,
                    "Anchar Notification", NotificationManager
                    .IMPORTANCE_HIGH)
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notification from Anchar");
            mNotifyManager!!.createNotificationChannel(notificationChannel);
        }
    }

    fun getNotificationBuilder(context: Context, title: String, text: String): NotificationCompat.Builder {
        val notifyBuilder = NotificationCompat.Builder(context, PRIMARY_CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(R.drawable.notify)
                .setStyle(NotificationCompat.BigTextStyle())
        return notifyBuilder
    }

    fun notifyMe(context: Context) {
        val notifyBuilder = getNotificationBuilder(context, "", "")
        if (mNotifyManager == null) {
            createNotificationChannel(context)
        }
        mNotifyManager!!.notify(1, notifyBuilder.build())
    }

    fun notifyChargingComplete(context: Context, contract: Contract) {
        val text = context.resources.getString(R.string.notifyText, contract.getDate(), contract.formatDuration(), contract.getEndPrize())
        val title = context.resources.getString(R.string.notifyTitle, contract.name)
        val notifyBuilder = getNotificationBuilder(context, title, text)
        if (mNotifyManager == null) {
            createNotificationChannel(context)
        }
        mNotifyManager!!.notify(1, notifyBuilder.build())
    }
}
