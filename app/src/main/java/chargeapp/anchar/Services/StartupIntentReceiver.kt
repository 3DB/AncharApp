package chargeapp.anchar.Services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class StartupIntentReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val startServiceIntent = Intent(context, ChargingUpdateService::class.java)
        context.startService(startServiceIntent)
    }
}
