package chargeapp.anchar.Services

import android.app.IntentService
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.util.Log
import chargeapp.anchar.R
import chargeapp.anchar.models.User
import chargeapp.anchar.util.Notifactions
import chargeapp.anchar.util.http.HTTPMethods
import chargeapp.anchar.util.http.HomeServer

private const val ACTION_BOOT = "chargeapp.chargeapp.activities.Services.action.FOO"

/**
 * Fetches Updates while awaiting a new Result of a Charging Process.
 */
class ChargingUpdateService : IntentService("ChargingUpdateService") {

    override fun onHandleIntent(intent: Intent?) {
        val homeserver = HomeServer(this)

        while (true) {
            if(User.isOneUserSaved(this)) {
                try {
                    homeserver.putContractsInDB {
                        val noti = Notifactions()
                        noti.notifyChargingComplete(this, it)
                    }
                } catch (e: Exception) {
                    Log.i("chargeapp/Service", e.message)
                }
            }
            Thread.sleep(2000)
        }
    }
}
