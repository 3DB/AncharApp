package chargeapp.anchar.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import org.json.JSONObject

@Entity(tableName = "Station")
class Station (
        @PrimaryKey var authorityId: Int,
        @PrimaryKey var stationId: Int,
        var gpsLatitude: Float,
        var gpsLongitude: Float,
        var activityState: String,
        var infoAddress: String
) {
    companion object {
        fun fromJson(json: JSONObject) {
            Station(
                    json.getInt("authority_id"),
                    json.getInt("station_id"),
                    json.getDouble("gps_latitude").toFloat(),
                    json.getDouble("gps_longitude").toFloat(),
                    json.getString("info_address"),
                    json.getString("activity_state")
            )
        }
    }
}
