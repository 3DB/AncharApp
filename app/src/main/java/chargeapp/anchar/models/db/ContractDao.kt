package chargeapp.anchar.models.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import chargeapp.anchar.models.Contract


@Dao
interface ContractDao {
    @Query("SELECT * FROM contract")
    fun getAll(): List<Contract>

    @Insert
    fun insert(vararg contracts: Contract)

    @Delete
    fun delete(contract: Contract)

    @Query("SELECT COUNT(*) FROM contract")
    fun getRowCount(): Int

    @Query("DELETE FROM contract")
    fun nukeTable()

    @Query("SELECT * FROM contract WHERE final_hash = :finalhash")
    fun getContractWithFinalHash(finalhash: String): List<Contract>
}
