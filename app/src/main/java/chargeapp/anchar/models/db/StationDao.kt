package chargeapp.anchar.models.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import chargeapp.anchar.models.Contract
import chargeapp.anchar.models.Station

@Dao
interface StationDao {
    @Query("SELECT * FROM station")
    fun getAll(): List<Station>

    @Insert
    fun insert(vararg station: Station)

    @Delete
    fun delete(vararg station: Station)

    @Query("DELETE FROM contract")
    fun nukeTable()

    @Query("SELECT * FROM station WHERE stationId = :STATIONID and authorityId = :authorityID")
    fun getContractByID(stationID: Int, authorityID: Int)

}
