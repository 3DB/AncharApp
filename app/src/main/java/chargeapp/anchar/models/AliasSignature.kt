package chargeapp.anchar.models

import org.json.JSONObject
import java.io.Serializable

class AliasSignature(val alias: String,
                     val authority_signature: Signature? = null,
                     val time_signed: Int = 0): Serializable  {

    var hash: String? = null
    var contract_signature: String? = null

    companion object {
        fun fromJson(json: JSONObject): AliasSignature{
            val alias = json.getString("alias")
            val time_signed = json.getInt("time_signed")
            //TODO What if none?
            val authoritySignatureJson = JSONObject(json.getString("authority_signature"))
            val aliasSig = AliasSignature(alias, Signature.fromJson(authoritySignatureJson), time_signed)
            aliasSig.run {
                if (!json.isNull("hash"))
                    hash = json.getString("hash")
                if (!json.isNull("contract_signature")) {
                    contract_signature = json.getString("contract_signature")
                }
            }
            return aliasSig
        }
    }

    fun toJson(): JSONObject
    {
        val json = JSONObject()
        json.put("alias", alias)
        if (authority_signature != null)
            json.put("authority_signature", authority_signature.toJson().toString())
        else
            json.put("authority_signature", JSONObject.NULL)
        json.put("time_signed", time_signed)
        if (contract_signature == null) {
            json.put("contract_signature", JSONObject.NULL)
        } else {
            json.put("contract_signature", contract_signature)
        }
        if (hash == null) {
            json.put("hash", JSONObject.NULL)
        } else {
            json.put("hash", hash)
        }

        return json
    }
}
