package chargeapp.anchar.models

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import chargeapp.anchar.util.Crypto
import chargeapp.anchar.util.http.HTTPMethods
import com.android.volley.VolleyError
import org.json.JSONObject


/**
 * Use User only with User.getUser() and User.createUser()
 */
class User(val username: String,
           val password: String,
           val network: String,
           val context: Context,
           val http: HTTPMethods = HTTPMethods.getInstance(context))
{ companion object {

    /**
     * create Instance of one User object from SharedPreferences
     * @throws RuntimeException Throws Exception when no user is saved
     */
    fun getUserFromSharedPreferences(context: Context): User {
        val sp: SharedPreferences =  context.getSharedPreferences("login", Context.MODE_PRIVATE) // TODO change to unique
        val emailSP = sp.getString(Const.emailString, "")
        val networkSP = sp.getString(Const.networkString, "")
        val passwordSP = sp.getString(Const.passwordString, "")
        Log.d("User","emailSP $emailSP, networkSP $networkSP, passwordSP $passwordSP")
        if (emailSP.isNotEmpty() && networkSP.isNotEmpty() && passwordSP.isNotEmpty()){
            return User(emailSP, networkSP, passwordSP, context)
        }
        throw RuntimeException("No user saved in SharedPreferences")
    }

    /**
     * Checks if one User is saved in SharedPreferences
     */
     fun isOneUserSaved(context: Context): Boolean {
        val sp: SharedPreferences =  context.getSharedPreferences("login", Context.MODE_PRIVATE)
        val emailSP = sp.getString(Const.emailString, "")
        val networkSP = sp.getString(Const.networkString, "")
        val passwordSP = sp.getString(Const.passwordString, "")
        Log.i("is email empty", emailSP.isEmpty().toString())
        return (emailSP.isNotEmpty() && networkSP.isNotEmpty() && passwordSP.isNotEmpty())
    }


    /**
     * creates a new User object
     * @throws RuntimeException Throws Exception when one user is already saved
     */
    fun createNewUser(context: Context, username: String, password: String, network: String): User
    {
        Crypto.genKeyPair(username)
        if (isOneUserSaved(context)) {
            throw RuntimeException("One user is already saved")
        }
        return User(username,
                Crypto.hash(password),
                network,
                context)
    }

    fun deleteUserFromSharedPreferences(context: Context) {
        if (!isOneUserSaved(context)) {
            throw RuntimeException("No user was saved")
        } else {
            val sp: SharedPreferences =  context.getSharedPreferences("login", Context.MODE_PRIVATE)
            val spe: SharedPreferences.Editor = sp.edit()
            spe.clear()
            spe.apply()
        }
    }
}

    fun register(errorHandler: (error: VolleyError) -> Unit, completionHandler: (response: JSONObject) -> Unit)
    {
        val param = JSONObject()
        param.put("username", username)
        param.put("password", password)
        param.put("pubkey", getKeyPair())
        http.post(network, "/register_user", param, errorHandler = {
            error -> errorHandler(error)
        }) {
            it -> completionHandler(it)
        }
    }

    fun login(errorHandler: (error: VolleyError) -> Unit, completionHandler: (response: JSONObject) -> Unit)
    {
        val param = JSONObject()
        http.post(network, "/login", param, sendAuth = true, authPassword = password, authUsername = username, errorHandler = {
            error -> errorHandler(error)
        }) {
            it -> completionHandler(it)
        }
    }

    fun getEmail(context: Context): String {
        val sp: SharedPreferences =  context.getSharedPreferences("login", Context.MODE_PRIVATE)
        return sp.getString(Const.emailString, "")
    }

    fun getPassowrd(context: Context): String {
        val sp: SharedPreferences =  context.getSharedPreferences("login", Context.MODE_PRIVATE)
        return sp.getString(Const.passwordString, "")
    }

    fun getNetwork(context: Context): String {
        val sp: SharedPreferences =  context.getSharedPreferences("login", Context.MODE_PRIVATE)
        return sp.getString(Const.networkString, "")
    }

    fun getKeyPair(): String {
        return Crypto.getPubKeyBase64(username)
    }

    /**
     * Saves user to SharedPreferences
     */
    fun save()
    {
        val sp: SharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE)
        val spe: SharedPreferences.Editor = sp.edit()
        spe.clear()
        spe.putString(Const.networkString, network)
        spe.putString(Const.emailString, username)
        spe.putString(Const.passwordString, password)
        spe.apply()
    }
}