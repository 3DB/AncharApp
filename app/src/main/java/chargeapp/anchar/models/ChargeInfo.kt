package chargeapp.anchar.models

import org.json.JSONObject

class ChargeInfo(
        val chargedPower: Double,
        val curAmp: Double,
        val curVolt: Double,
        val curPower: Double,
        val chargingDuration: Long,
        val startTime: Long
)  {
    companion object {

        fun fromJson(json: JSONObject): ChargeInfo {
            return ChargeInfo(
                    json.getDouble( "charged_power"),
                    json.getDouble("cur_amp"),
                    json.getDouble("cur_volt"),
                    json.getDouble("cur_power"),
                    json.getLong("duration"),
                    json.getLong("start_time")
            )
        }
    }

    fun toJson(): JSONObject {
        val json: JSONObject = JSONObject()
        json.put("charged_power", chargedPower)
        json.put("cur_amp", curAmp)
        json.put("cur_volt", curVolt)
        json.put("duration", chargingDuration)
        json.put("cur_power", curPower)
        json.put("start_time", startTime)
        return json
    }

}