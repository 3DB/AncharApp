package chargeapp.anchar.models

import android.arch.persistence.room.*
import chargeapp.anchar.util.TimeUtils
import org.json.JSONObject
import java.io.Serializable

@Entity(tableName = "contract")
data class Contract(@ColumnInfo(name = "additions") val additions: Double,
                    @ColumnInfo(name = "price_per_kwh") val pricePerKwh: Double,
                    @ColumnInfo(name = "station_id") val stationId: Int,
                    @ColumnInfo(name = "user_hash") val userHash: String,
                    @ColumnInfo(name = "start_timestamp") val startTimestamp: Long,
                    @ColumnInfo(name = "name") var name: String = "JuFo Chemnitz"): Serializable{

    @PrimaryKey(autoGenerate = true) var id = 0
    @Ignore var userSignature: AliasSignature? = null // (prefix = "user_")
    @ColumnInfo(name = "charging_amount") var chargingAmount: Double? = null
    @ColumnInfo(name = "end_timestamp") var endTimestamp: Int? = null
    @ColumnInfo(name = "final_hash") var finalHash: String? = null
    @Ignore var finalSignature: Signature? = null // (prefix = "final_")
    @Ignore val multiplyTime = true

    companion object {
        fun fromJson(json: JSONObject): Contract
        {
            val contract = Contract(
                    json.getDouble("additions"),
                    json.getDouble("price_per_kwh"),
                    json.getInt("station_id"),
                    json.getString("user_hash"),
                    json.getLong("start_timestamp"))

            if (!json.isNull("user_signature"))
                contract.userSignature = AliasSignature
                        .fromJson(JSONObject(json.getString("user_signature")))
            if (!json.isNull("final_signature"))
                contract.finalSignature = Signature
                        .fromJson(JSONObject(json.getString("final_signature")))

            if (!json.isNull("charging_amount"))
                contract.chargingAmount = json.getDouble("charging_amount")
            if (!json.isNull("end_timestamp"))
               contract.endTimestamp = json.getInt("end_timestamp")
            if (!json.isNull("final_hash"))
                contract.finalHash = json.getString("final_hash")

            return contract
        }
    }

    fun getDate(): String{
        return TimeUtils.toDate(startTimestamp)
    }

    fun getDuration(): Int {
        return (this.endTimestamp!! - this.startTimestamp).toInt()
    }

    fun getEndPrize(): Double {
        return (this.pricePerKwh * this.chargingAmount!!) / 1000
    }

    fun formatDuration(): String {
        var absSeconds = Math.abs(endTimestamp!! - startTimestamp)
        val str = String.format(
                "%d Std. %02d Min.",
        absSeconds / 3600,
        (absSeconds % 3600) / 60)
        return str
    }

}

