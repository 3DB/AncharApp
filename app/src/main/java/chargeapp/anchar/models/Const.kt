package chargeapp.anchar.models

class Const {
    companion object {
        val networkString = "network"
        val emailString = "email"
        val passwordString = "password"
    }
}