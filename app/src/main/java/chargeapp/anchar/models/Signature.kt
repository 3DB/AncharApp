package chargeapp.anchar.models

import org.json.JSONObject
import java.io.Serializable

class Signature(val roleID: Int,
                val signature: String): Serializable{
    companion object {
        fun fromJson(json: JSONObject): Signature
        {
            val roleId = json.getInt("role_id")
            val signature = json.getString("signature")
            return Signature(roleId, signature)
        }
    }
    fun toJson(): JSONObject
    {
        val json = JSONObject()
        json.put("role_id", roleID)
        json.put("signature", signature)
        return json
    }
}
