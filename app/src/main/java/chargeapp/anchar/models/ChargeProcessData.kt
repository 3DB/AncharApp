package chargeapp.anchar.models


import chargeapp.anchar.util.ChargeProcess
import chargeapp.anchar.util.Crypto
import java.io.Serializable

class ChargeProcessData(
        val ipAddressForeignServer: String,
        val ipAddressHomeServer: String,
        val stationId: Int,
        val keyAlias: String,
        val randID: String,
        val token: String = ChargeProcess.tokenGenerator()
        ) : Serializable {
    var aliasSignature: AliasSignature? = null
    var contract: Contract? = null


    init {
        Crypto.genKeyPair(keyAlias)
        aliasSignature = AliasSignature(Crypto.getPubKeyBase64(keyAlias))
    }
}
