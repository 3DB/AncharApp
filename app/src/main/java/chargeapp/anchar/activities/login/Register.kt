package chargeapp.anchar.activities.login

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.widget.Button
import android.widget.Toast
import chargeapp.anchar.R
import chargeapp.anchar.activities.MainActivity
import chargeapp.anchar.models.User

class Register : AppCompatActivity() {

    private lateinit var network: TextInputEditText
    private lateinit var password: TextInputEditText
    private lateinit var username: TextInputEditText
    private lateinit var registerButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        network = findViewById(R.id.input_network_register)
        password = findViewById(R.id.input_password_register)
        username = findViewById(R.id.input_email_register)
        registerButton = findViewById(R.id.button_register)

        registerButton.setOnClickListener {
            try {
                val user = User.createNewUser(
                        this,
                        username.text.toString(),
                        password.text.toString(),
                        network.text.toString())
                user.register({
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }) {
                    user.save()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }
            } catch (e: RuntimeException) {
                Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

}
