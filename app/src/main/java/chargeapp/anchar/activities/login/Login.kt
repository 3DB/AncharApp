package chargeapp.anchar.activities.login

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import chargeapp.anchar.R
import chargeapp.anchar.activities.MainActivity
import chargeapp.anchar.models.User


class Login : AppCompatActivity() {

    private lateinit var inputLoginButton: Button
    private lateinit var inputNetwork: EditText
    private lateinit var inputEmail: EditText
    private lateinit var inputPassword: EditText
    private lateinit var inputNoaccount: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        inputLoginButton = findViewById(R.id.login_button)
        inputNetwork = findViewById(R.id.input_login_network)
        inputEmail = findViewById(R.id.input_login_email)
        inputPassword = findViewById(R.id.input_login_password)
        inputNoaccount = findViewById(R.id.noAccount)

        if (User.isOneUserSaved(this)) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        inputNoaccount.setOnClickListener {
            val intent = Intent(this, Register::class.java)
            startActivity(intent)
        }

        inputLoginButton.setOnClickListener {
            val email = inputEmail.text.toString()
            val password = inputPassword.text.toString()
            val network = inputNetwork.text.toString()

            when {
                email.isEmpty() -> Toast.makeText(this, resources.getText(R.string.toast_email_empty), Toast.LENGTH_LONG)
                network.isEmpty() -> Toast.makeText(this, resources.getText(R.string.toast_password_empty), Toast.LENGTH_LONG)
                network.isEmpty() -> Toast.makeText(this, resources.getText(R.string.toast_password_empty), Toast.LENGTH_LONG)
            }

            val user = User.createNewUser(
                    this,
                    email,
                    password,
                    network)
            user.login( {
                Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
            }) {
                user.save()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                /*
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent,
                            ActivityOptions.makeSceneTransitionAnimation(this).toBundle())

                } else {

                }
                */
            }
        }
    }

    override fun onBackPressed() {
    }

}
