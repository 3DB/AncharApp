package chargeapp.anchar.activities.ChargeProccess

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Button
import chargeapp.anchar.R
import chargeapp.anchar.activities.MainActivity
import chargeapp.anchar.activities.fragments.ContractDetailsFragment
import chargeapp.anchar.models.ChargeProcessData
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.ChargeProcess
import kotlinx.android.synthetic.main.activity_contract_details.*

class ContractDetails : AppCompatActivity(), ContractDetailsFragment.OnFragmentInteractionListener {

    private lateinit var chargeProcess: ChargeProcess
    private lateinit var contract: Contract
    private lateinit var acceptButton: Button
    private lateinit var declineButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contract_details)

        supportActionBar?.title = resources.getString(R.string.title_contract_details)

        chargeProcess = ChargeProcess(this.applicationContext, intent.getSerializableExtra(AcceptActivity.DATA) as ChargeProcessData)
        contract = intent.getSerializableExtra(AcceptActivity.CONTRACT) as Contract

        acceptButton = findViewById(R.id.accept_contract_button)
        declineButton = findViewById(R.id.decline_contract_button)

        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val args = Bundle()
        args.putSerializable("contract", contract)
        val fragment = ContractDetailsFragment.newInstance(contract)
        fragmentTransaction.add(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()

        acceptButton.setOnClickListener {
            this.accept_progress.visibility = View.VISIBLE
            accept_contract_button.isClickable = false
            accept_contract_button.setBackgroundColor(Color.parseColor("#808080"))

            chargeProcess.requestAliasAndAccept({
                this.accept_progress.visibility = View.INVISIBLE
                Snackbar.make(contract_details_constraint, R.string.error_begin_charging, Snackbar.LENGTH_SHORT).show()
            }) {
                this.accept_progress.visibility = View.INVISIBLE
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra(MainActivity.workingAuth, true)
                startActivity(intent)
            }

            /*
            val intent = Intent(this, AcceptActivity::class.java)
            intent.putExtra(AcceptActivity.CONTRACT, contract)
            intent.putExtra(AcceptActivity.DATA, chargeProcess.data)
            startActivity(intent)
            */
        }

        declineButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onFragmentInteraction(uri: Uri) {
    }

}
