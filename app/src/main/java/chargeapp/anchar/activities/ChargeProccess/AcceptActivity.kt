package chargeapp.anchar.activities.ChargeProccess

import android.content.Context
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.view.inputmethod.InputMethodManager
import chargeapp.anchar.R
import chargeapp.anchar.models.ChargeProcessData
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.ChargeProcess
import kotlinx.android.synthetic.main.activity_accept.*


class AcceptActivity: AppCompatActivity(){
    companion object {
        val DATA = "0"
        val CONTRACT = "1"
    }

    private lateinit var ipAddressHome: TextView
    private lateinit var randInt: TextView
    private lateinit var chargeProcess: ChargeProcess
    private lateinit var contract: Contract
    private lateinit var startChargeButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accept)

        supportActionBar?.title = resources.getString(R.string.title_accept_activity)
        randInt = findViewById(R.id.randInt)
        chargeProcess = ChargeProcess(this.applicationContext, intent.getSerializableExtra(DATA) as ChargeProcessData)
        contract = intent.getSerializableExtra(CONTRACT) as Contract
        startChargeButton = findViewById(R.id.accept_startCharge)

        randInt.requestFocus()
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        randInt.setOnKeyListener { v, keyCode, event ->
            when (keyCode) {
                KeyEvent.KEYCODE_ENTER -> startCharge()
                KeyEvent.KEYCODE_NUMPAD_ENTER -> startCharge()
            }
            false
        }

        startChargeButton.setOnClickListener {
            startCharge()
        }

    }

    fun startCharge()
    {
        accept_startCharge.isClickable = false
        accept_startCharge.setBackgroundColor(Color.parseColor("#808080"));
        Log.i("AcceptActivity", "StartCharge")
        this.accept_progress.visibility = View.VISIBLE

        /*
        chargeProcess.requestAliasAndAccept(randInt.text.toString().toInt(), {
            it ->
                this.accept_progress.visibility = View.INVISIBLE
                Snackbar.make(this.coordinatorLayout, "Error", Snackbar.LENGTH_LONG).show() // TODO welcher Error code für falschen randID
        }){
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(MainActivity.workingAuth, true)
            startActivity(intent)
        }
        */
    }
}
