package chargeapp.anchar.activities.fragments

import android.content.Context
import android.os.Bundle
import android.app.Fragment
import android.content.Intent
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import chargeapp.anchar.ContractHistoryRecyclerViewAdapter
import chargeapp.anchar.R
import chargeapp.anchar.activities.ContractDetailsHistory
import chargeapp.anchar.DataObjects.ContractContent
import chargeapp.anchar.Services.ChargingUpdateService
import chargeapp.anchar.models.Contract
import kotlinx.android.synthetic.main.fragment_contract_list.*
import kotlinx.android.synthetic.main.fragment_contract_list.view.*

class HistoryFragment : Fragment() {

    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contract_list, container, false)

        val startServiceIntent = Intent(context, ChargingUpdateService::class.java)
        context.startService(startServiceIntent)
        // Set the adapter
        if (view.list is RecyclerView) {
            with(view.list) {
                view.list.layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                adapter = ContractHistoryRecyclerViewAdapter(ContractContent.ITEMS, activity, {
                    onDataChanged()
                }) {
                    val intent = Intent(activity, ContractDetailsHistory::class.java)
                    intent.putExtra(ContractDetailsHistory.CONTRACT, it)
                    startActivity(intent)
                }
            }
        }
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Contract?) {
        }
    }

    fun onDataChanged() {
        this.progressBar?.visibility = View.INVISIBLE
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
                HistoryFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }

}
