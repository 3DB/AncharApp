package chargeapp.anchar.activities.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import chargeapp.anchar.R
import chargeapp.anchar.models.Contract
import kotlinx.android.synthetic.main.fragment_contract_details.view.*


class ContractDetailsFragment : Fragment() {
    var contract: Contract? = null
    private var listener: OnFragmentInteractionListener? = null

    private lateinit var stationIdText: TextView
    private lateinit var additionsText: TextView
    private lateinit var pricePerKwhText: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_contract_details, container, false)
        stationIdText = view.findViewById(R.id.station_id_content)
        additionsText = view.findViewById(R.id.additions_content)
        pricePerKwhText = view.findViewById(R.id.price_per_kwh_content)

        stationIdText.text = contract!!.name
        additionsText.text = resources.getString(R.string.prize, contract!!.additions)
        pricePerKwhText.text = resources.getString(R.string.prize, contract!!.pricePerKwh)

        if(contract!!.endTimestamp != null) {
            view.contract_details_duration.visibility = View.VISIBLE
            view.contract_details_duration_content.visibility = View.VISIBLE
            view.contract_details_duration_content.text = contract!!.formatDuration()
            view.divider7.visibility = View.VISIBLE
            view.endPrize.visibility = View.VISIBLE
            view.endPrize.text = resources.getString(R.string.endPrize, contract!!.getEndPrize())
            view.contract_sum.visibility = View.VISIBLE
            view.divider14.visibility = View.VISIBLE
            view.contract_energy.visibility = View.VISIBLE
            view.contract_energy_content.visibility = View.VISIBLE
            view.contract_energy_content.text = resources.getString(R.string.energy_number, contract!!.chargingAmount)
        }

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {

        @JvmStatic
        fun newInstance(contract: Contract): ContractDetailsFragment {
            val cdf = ContractDetailsFragment()
            cdf.contract = contract
            return cdf
        }
    }
}
