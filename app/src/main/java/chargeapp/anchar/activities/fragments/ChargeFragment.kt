package chargeapp.anchar.activities.fragments

import android.Manifest
import chargeapp.anchar.R
import chargeapp.anchar.activities.ChargeProccess.AcceptActivity
import chargeapp.anchar.activities.ChargeProccess.ContractDetails
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.ChargeProcess
import chargeapp.anchar.models.User
import android.app.Fragment
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import chargeapp.anchar.activities.UtilActivities.ScanActivity
import kotlinx.android.synthetic.main.activity_accept.*
import kotlinx.android.synthetic.main.fragment_charge.view.*

class ChargeFragment : Fragment() {

    private lateinit var ipAddressEdit: EditText
    private lateinit var stationId: EditText
    private lateinit var startAuth: Button
    private val REQEUSTCODE = 10;


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_charge, container, false)

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(activity,
                    arrayOf(Manifest.permission.CAMERA),
                    REQEUSTCODE
                    )
        }

        startAuth = view.findViewById<View>(R.id.startAuth) as Button
        ipAddressEdit = view.findViewById<View>(R.id.ipAddress) as EditText
        stationId = view.findViewById<View>(R.id.stationId) as EditText

        startAuth.setOnClickListener{
            startAuth()
        }
        stationId.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                startAuth()
            }
            false
        }

        view.qrButton.setOnClickListener {
            val intent = Intent(activity, ScanActivity::class.java)
            startActivity(intent)
        }

        return view
    }

    fun startAuth()
    {
        val network = User.getUserFromSharedPreferences(activity).getNetwork(activity)

        if (stationId.text.toString().toIntOrNull() != null) {
            val chargeProcess = ChargeProcess.newInstance( // TODO randID nachpatchen!
                    network,
                    ipAddressEdit.text.toString(),
                    stationId.text.toString().toInt(),
                    "",// TODO What if bad input
                    activity)

            chargeProcess.beginCharging( {
                error ->
                Log.i("HTTPMethods", "Auth failed")
                Snackbar.make(view.findViewById(R.id.frameLayout), "Auth failed", Snackbar.LENGTH_LONG)
                        .show()
            }) {
                response: Contract ->
                val intent = Intent(activity, ContractDetails::class.java)
                intent.putExtra(AcceptActivity.CONTRACT, response)
                intent.putExtra(AcceptActivity.DATA, chargeProcess.data)
                startActivity(intent)
            }
        } else {
            Snackbar.make(coordinatorLayout, R.string.error_malformed_stationID, Snackbar.LENGTH_LONG)
        }

    }

    companion object {

        fun newInstance(): ChargeFragment {

            return ChargeFragment()
        }
    }
}