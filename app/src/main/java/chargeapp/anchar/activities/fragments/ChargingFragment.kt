package chargeapp.anchar.activities.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import chargeapp.anchar.R
import chargeapp.anchar.models.ChargeInfo
import chargeapp.anchar.util.TimeUtils
import kotlinx.android.synthetic.main.fragment_charging.view.*
import org.json.JSONObject

private const val ARG_PARAM1 = "param1"


class ChargingFragment : Fragment() {
    private var chargeInfo: ChargeInfo? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            chargeInfo = ChargeInfo.fromJson(JSONObject(it.getString(ARG_PARAM1)))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val lf = inflater.inflate(R.layout.fragment_charging, container, false)
        lf.charging_point.setText("JuFo Chemnitz")
        lf.text_voltage.text = resources.getString(R.string.voltage_number, chargeInfo!!.curVolt)
        lf.text_current.text = resources.getString(R.string.current_number, chargeInfo!!.curAmp)
        lf.text_power.text = resources.getString(R.string.power_number, chargeInfo!!.curPower)
        lf.text_energy.text = resources.getString(R.string.energy_number, chargeInfo!!.chargedPower)
        lf.text_duration.text = TimeUtils.formatDuration(chargeInfo!!.chargingDuration)
        lf.text_starttimestamp.text = TimeUtils.toDate(chargeInfo!!.startTime)
        lf.progressBar2.progress = 25
        return lf
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: ChargeInfo): ChargingFragment {
            val cf = ChargingFragment()
            cf.apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1.toJson().toString(0))
                }
            }
            return cf
        }
    }
}
