package chargeapp.anchar.activities.fragments


import android.Manifest
import android.app.Fragment
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import chargeapp.anchar.R
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.views.MapView;
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.fragment_settings.view.*
import org.osmdroid.config.Configuration
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.*


class MapFragment : Fragment() {

    private lateinit var map: MapView;
    val REQUEST_PERM = 21313

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_settings, container, false)

        if (ContextCompat.checkSelfPermission(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(activity,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_PERM)

            }
        }

            val ctx = activity.getApplicationContext()
            Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx))
            map = v.map
            map.setTileSource(TileSourceFactory.MAPNIK)
            map.setMultiTouchControls(true);
            val mapController = map.controller
            mapController.setZoom(9)
            val startPoint = GeoPoint(51.9555423, 7.6273813)
            mapController.setCenter(startPoint)


        addMarker("E-Tankstelle Am Ludgeriplatz", "Stadtwerke Münster", 51.9555423, 7.6273813, resources.getDrawable(R.drawable.car_electric_station_small))
        addMarker("JuFo Leverkusen", "Anchar", 51.012862, 6.981010, resources.getDrawable(R.drawable.car_electric_station_small))


        return v
    }

    fun addMarker(name: String, desc: String, latitude: Double, longitude: Double, draw: Drawable) {
        val m = Marker(map);
        draw.setBounds(12, 12, 12, 12)
        m.setIcon(draw);
        m.setTitle(name);
        m.subDescription = desc

        val geo = GeoPoint(latitude, longitude);
        m.setPosition(geo);
        m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        map.getOverlays().add(m);

        m.setOnMarkerClickListener { marker, mapView ->
            mapView.controller.animateTo(marker.position, 18.0, 1000)
            marker.showInfoWindow()
            true
        }
    }

    override fun onPause() {
        super.onPause()
        map.onPause()
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }


    companion object {
        fun newInstance(): MapFragment {
            return MapFragment()
        }
    }
}
