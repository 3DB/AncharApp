package chargeapp.anchar.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import chargeapp.anchar.R

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
