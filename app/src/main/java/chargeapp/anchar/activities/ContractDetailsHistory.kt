package chargeapp.anchar.activities

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import chargeapp.anchar.R
import chargeapp.anchar.activities.fragments.ContractDetailsFragment
import chargeapp.anchar.models.Contract

class ContractDetailsHistory : AppCompatActivity(), ContractDetailsFragment.OnFragmentInteractionListener {
    companion object {
        val CONTRACT = "1"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contract_details_history)

        supportActionBar?.title = resources.getString(R.string.title_history)

        val contract = intent.getSerializableExtra(CONTRACT) as Contract
        val contractFragment = ContractDetailsFragment.newInstance(contract)
        supportFragmentManager.beginTransaction().add(android.R.id.content, contractFragment).commit()
    }

    override fun onFragmentInteraction(uri: Uri)
    {

    }
}
