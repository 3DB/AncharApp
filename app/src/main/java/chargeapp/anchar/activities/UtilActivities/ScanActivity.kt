package chargeapp.anchar.activities.UtilActivities

import android.content.Intent
import com.google.zxing.Result
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.widget.Toast
import chargeapp.anchar.activities.ChargeProccess.AcceptActivity
import chargeapp.anchar.activities.ChargeProccess.ContractDetails
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.ChargeProcess
import chargeapp.anchar.models.User
import me.dm7.barcodescanner.zxing.ZXingScannerView
import org.json.JSONObject

class ScanActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private var mScannerView: ZXingScannerView? = null

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        mScannerView!!.setResultHandler(this)
        setContentView(mScannerView)                // Set the scanner view as the content view#
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
    }

    override fun handleResult(rawResult: Result) {

        val network = User.getUserFromSharedPreferences(this).getNetwork(this)
        Log.d("ChargeApp/handleResult", rawResult.text)
        val json = JSONObject(rawResult.text)
        val chargeProcess = ChargeProcess.newInstance(
                network,
                json.getString("url"),
                json.getInt("stationID"),
                json.getString("rand_id"),
                this)

        chargeProcess.beginCharging( {
            error ->
            Log.i("Scan Activity", error.toString())
            Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show()
        }) {
            response: Contract ->
            val intent = Intent(this, ContractDetails::class.java)
            intent.putExtra(AcceptActivity.CONTRACT, response)
            intent.putExtra(AcceptActivity.DATA, chargeProcess.data)
            startActivity(intent)
        }
    }

}
