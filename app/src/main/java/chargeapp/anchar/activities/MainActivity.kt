package chargeapp.anchar.activities

import android.app.AlertDialog
import android.app.Fragment
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import chargeapp.anchar.R
import chargeapp.anchar.activities.fragments.ChargeFragment
import chargeapp.anchar.activities.fragments.ChargingFragment
import chargeapp.anchar.activities.fragments.HistoryFragment
import chargeapp.anchar.activities.fragments.MapFragment
import chargeapp.anchar.activities.login.Login
import chargeapp.anchar.models.ChargeInfo
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.Crypto
import chargeapp.anchar.models.User
import chargeapp.anchar.util.ChargeProcess
import chargeapp.anchar.util.db.ContractRepo
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity(), HistoryFragment.OnListFragmentInteractionListener {
    companion object {
    val workingAuth = "Working"
}

    private var checkingForChargingData = false // will be set on startCharging

    private fun setupBottomNavigation() {

        val mBottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        mBottomNavigationView.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {

                R.id.navigation_home -> {
                    startCheckingCharging()
                    loadHomeFragment()
                    return@OnNavigationItemSelectedListener true
                }

                R.id.navigation_dashboard -> {
                    checkingForChargingData = false
                    loadContractFragment()
                    return@OnNavigationItemSelectedListener true
                }

                R.id.navigation_notifications -> {
                    checkingForChargingData = false
                    loadMapFragment()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupBottomNavigation()
        if(intent.hasExtra("workingCharging")) {
            if(intent.getBooleanExtra(MainActivity.workingAuth, false)) {
                Snackbar.make(coordinatorLayout2, getText(R.string.finish), Snackbar.LENGTH_LONG).show()
            }
        }

        checkIfLoggedIn()

        if (savedInstanceState == null) {
            loadHomeFragment()
            startCheckingCharging()
        }
    }

    fun startCheckingCharging() {
        if(!checkingForChargingData) {
            checkingForChargingData = true
            doAsync {
                while (checkingForChargingData && ChargeProcess.isCharging(application) && User.isOneUserSaved(application)) {
                    ChargeProcess.getChargingInfo(application, errorHandler = {
                        if (it.networkResponse.statusCode == 404 && ChargeProcess.isDataReceived(application)) {
                            ChargeProcess.clearCharging(application)
                            uiThread {
                                loadHomeFragment()
                            }
                        }
                    }) {
                        chargeInfo ->
                        ChargeProcess.receivedData(application)
                        uiThread {
                            loadChargingFragment(chargeInfo)
                        }

                    }
                    Thread.sleep(2000)
                }
            }
        }
    }

    override fun onBackPressed() {
        logoutDialog()
    }

    override fun onResume() {
        super.onResume()
        checkIfLoggedIn()
    }

    fun checkIfLoggedIn() {
        if (!User.isOneUserSaved(this)) {
            Snackbar.make(coordinatorLayout2, resources.getString(R.string.not_logged_id), Snackbar.LENGTH_LONG).show()
            val intent = Intent(this, Login::class.java)
            startActivity(intent)
        }
    }

    fun logoutDialog() {
        AlertDialog.Builder(this) // TODO move this to a class
                .setTitle(resources.getString(R.string.logoutTitle))
                .setMessage(resources.getString(R.string.askForLogout))
                .setPositiveButton(android.R.string.yes) { _, _ ->
                    checkingForChargingData = false
                    User.deleteUserFromSharedPreferences(this)
                    Crypto.delKeys()
                    val intent = Intent(this, Login::class.java)
                    startActivity(intent)

                    /*
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(intent,
                                ActivityOptions.makeSceneTransitionAnimation(this).toBundle())

                    } else {
                        startActivity(intent)
                    }
                    */
                }
                .setNegativeButton(android.R.string.no, null).show()
    }

    private fun loadHomeFragment() {
        supportActionBar?.title = resources.getString(R.string.title_charge)
        val fragment = ChargeFragment.newInstance()
        val ft = fragmentManager.beginTransaction()
        ft.replace(R.id.fragment_frame, fragment)
        ft.commitAllowingStateLoss()
    }

    private fun loadChargingFragment(chargeInfo: ChargeInfo) {
        supportActionBar?.title = resources.getString(R.string.title_charging)
        val fragment: Fragment = ChargingFragment.newInstance(chargeInfo)
        val ft = fragmentManager.beginTransaction()
        ft.replace(R.id.fragment_frame, fragment)
        ft.commitAllowingStateLoss()
    }

    private fun loadContractFragment() {

        supportActionBar?.title = resources.getString(R.string.title_history)
        val fragment: Fragment = HistoryFragment.newInstance(1)
        val ft = fragmentManager.beginTransaction()
        ft.replace(R.id.fragment_frame, fragment)
        ft.commitAllowingStateLoss()
    }

    private fun loadMapFragment() {

        supportActionBar?.title = resources.getString(R.string.title_map)
        val fragment = MapFragment.newInstance()
        val ft = fragmentManager.beginTransaction()
        ft.replace(R.id.fragment_frame, fragment)
        ft.commitAllowingStateLoss()
    }

    override fun onListFragmentInteraction(item: Contract?) {
        Log.i("mainActivity", "pressed")
        if (item != null) {
            val intent = Intent(this, ContractDetailsHistory::class.java)
            intent.putExtra(ContractDetailsHistory.CONTRACT, item)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.mainmenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_logout -> {
                logoutDialog()
                true
            }

            R.id.menu_about -> {
                loadChargingFragment(ChargeInfo(80.0, 2.0, 40.0, 80.0, 3600, 1550420461))
                true
            }

            R.id.menu_delete_db -> {
                ContractRepo(this).deleteAll()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
