package chargeapp.anchar

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import chargeapp.anchar.models.Contract
import chargeapp.anchar.util.db.AppDatabase


import kotlinx.android.synthetic.main.fragment_contract.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class ContractHistoryRecyclerViewAdapter(
        private var mValues: List<Contract>,
        private val act: Activity,
        private val onDataSet: () -> Unit,
        private val mListener: (item: Contract) -> Unit)
    : RecyclerView.Adapter<ContractHistoryRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        doAsync {
            val contractDao = AppDatabase.getDatabase(act)!!.ContractDau()
            val contracts = contractDao.getAll()
            uiThread {
                setData(contracts)
            }
        }

        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Contract
            mListener(item)
        }
    }

    /*
    fun refill(context: Context) {

        val hs = HomeServer(context)
        hs.getContracts {
            setData(it)
        }
    }
    */

    fun setData(values: List<Contract>) {
        mValues = values.toMutableList().sortedByDescending { it.startTimestamp }
        this.notifyDataSetChanged()
        this.onDataSet()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_contract, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mIdView.text = item.getDate()
        holder.mContentView.text =
                this.act.resources.getString(R.string.prize, item.getEndPrize())
        holder.mDescription.text = item.name
        holder.mTimeView.text = this.act.resources.getString(R.string.energy_number, item.chargingAmount)

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View): RecyclerView.ViewHolder(mView) {
        val mIdView: TextView = mView.description
        val mContentView: TextView = mView.price
        val mDescription: TextView = mView.date
        val mTimeView: TextView = mView.time

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }

}
